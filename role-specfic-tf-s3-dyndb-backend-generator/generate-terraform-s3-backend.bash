#!/bin/bash

#####################################################
#
#  Written by Jerald McConnell 07/18/2020
#
#  Bash script generates heirchial and role specific
#  Terraform s3/dynamodb remote state+locking
#  back-end anible code that will create the remote
#  backends when invoked and provides an example 
#  backends.tf file for Terraform code that will
#  be assocated with this role.
#  
#
#  Invoke by editing and then  sourcing 
#  generate-terraform-s3-backend.ini
#  in CWD or file passed as $1
#  
#  Execute created code by:
#
#   1) Install Terraform version 0.12 or later
#   2) cd into resulting directory:
#      ${ROLE_DESTINATION_DIR}/files_to_generate_backend/
#   3) create providers.tf that references a valid aws credentials file
#   4) run: terraform init
#   5) run: terraform plan
#   6) run: terraform apply
#   7) backend can be used by adding backends.tf to your Terraform code which is stored in:
#          ${ROLE_DESTINATION_DIR}/files_to_generate_backend/backends.tf
#
#####################################################


###   source generate-terraform-s3-backend.ini

if [ -e "$1" ] ; then
     IS_IT_A_CONFIG=`cat $1 | grep TERRAFORM_S3_BACKEND_CONFIG_FILE=true | wc -l`

     if [ "${IS_IT_A_CONFIG}" -eq "1" ] ; then
         . $1
        SOURCED_VARS=true
     fi
fi

if [ -f "./generate-terraform-s3-backend.ini" ] ; then
         . ./generate-terraform-s3-backend.ini
        SOURCED_VARS=true
fi

if [ "${SOURCED_VARS}" != "true" ] ; then
    echo "ERROR: No default ./generate-terraform-s3-backend.ini or alternate config file found."
   echo "Exiting." 
exit 2
fi


############ Reference for sourced variables from: ./generate-terraform-s3-backend.ini
###     
###     ORG=jmlabs
###     DEPT=analytics
###     PROJ=newproj
###     WORKFLOW_STAGE=poc
###     SCOPE_NAME=vpc ### shoud be changed for seperate terraform runs for vpc, instance, etc
###     REGION=us-west-2
###     STATE_S3_REGION=${us-west-2:$REGION}
###     ROLE_DESTINATION_DIR=~/training/project/project1/newdeploy
###     AWS_CREDENTIALS_FILE=~/.aws/credentials-terraform
###     AWS_PROFILE=terraform
###     AWS_VERSION=2.7
###     TERRAFORM_S3_BACKEND_CONFIG_FILE=true ## bash script uses this to validate ini file
###     DATESTAMP=`date +%Y%m%d%H%M%S`
###     BASE62_DATESTAMP="" ### code below shortens DATESTAMP CHARS by base62 encoding
###     STATE_S3_BUCKET=${ORG}-${DEPT}-${PROJ}-tf-rmt-bknd-${BASE62_DATESTAMP}
###     S3_TF_STATE_KEY_PATH=${ORG}/${DEPT}/${PROJ}/${WORKFLOW_STAGE}/${WORKFLOW_STAGE}/${ROLE}/terraform.tfstate
###     DYNAMODB_LOCK_TABLE=${ORG}-${DEPT}-${PROJ}-${WORKFLOW_STAGE}-${WORKFLOW_STAGE}-${ROLE}-tf-locks-${BASE62_DATESTAMP}
### 
############# End source file reference





########   FUNCTIONS    ###################


create_dirs () {

     if [ ! -d "${ROLE_DESTINATION_DIR}" ] ; then
        mkdir -p ${ROLE_DESTINATION_DIR}/files_to_generate_backend
        mkdir -p ${ROLE_DESTINATION_DIR}/backend_files_for_new_terraform_code
        mkdir ${TMP_DIR}
     else
       echo "Directory ${ROLE_DESTINATION_DIR} alteady exists.  Exiting with error."
       exit 2
     fi
  
}


provider_tf_create () {
     echo "INFO: Creating provider.tf in ${ROLE_DESTINATION_DIR} subdirectories files_to_generate_backend and backend_files_for_new_terraform_code"
##### provider.tf.template
     cat << EOF > ${TMP_DIR}/provider.tf.template
provider "aws" {
  region                  = "REGION"
  shared_credentials_file = "AWS_CREDENTIALS_FILE"
  profile                 = "AWS_PROFILE"
  version                 = "AWS_VERSION"
}
EOF



cat  ${TMP_DIR}/provider.tf.template | sed -e "s/REGION/${REGION}/g" |  sed -e "s/AWS_PROFILE/${AWS_PROFILE}/g" | sed -e "s/AWS_VERSION/${AWS_VERSION}/g" | sed -e "s+AWS_CREDENTIALS_FILE+${AWS_CREDENTIALS_FILE}+g" >  ${TMP_DIR}/provider.tf

cp ${TMP_DIR}/provider.tf ${ROLE_DESTINATION_DIR}/files_to_generate_backend/
cp ${TMP_DIR}/provider.tf ${ROLE_DESTINATION_DIR}/backend_files_for_new_terraform_code/


}

##   terraform-dynamodb.tf
terraform_dynamodb_tf_create () {
     echo "INFO: Copying terraform-dynamodb.tf file to ${ROLE_DESTINATION_DIR}/files_to_generate_backend/ "

     cat << EOF > ${TMP_DIR}/'terraform-dynamodb.tf.template'
resource "aws_dynamodb_table" "terraform-dynamodb-tf-lock" {
  name           = var.dynamodb_lock_table
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "\${var.state_s3_bucket}-table"
    Organization    = var.org
    Department      = var.dept
    Project         = var.project
    Workflow-stage  = var.workflow_stage
    Terraform-Scope = var.scope_name
  }

}
EOF

cp ${TMP_DIR}/terraform-dynamodb.tf.template ${ROLE_DESTINATION_DIR}/files_to_generate_backend/terraform-dynamodb.tf

}

##   terraform-s3-state.tf
terraform_s3_state_tf_create () {
     echo "INFO: Copying terraform-s3-state.tf file to ${ROLE_DESTINATION_DIR}/files_to_generate_backend/ "

     cat << EOF > ${TMP_DIR}/'terraform-s3-state.tf.template'
resource "aws_s3_bucket" "state-s3-bucket" {
  bucket = var.state_s3_bucket
  acl    = "private"
  region = var.state_s3_region

  tags = {
    Name            = var.state_s3_bucket
    Organization    = var.org
    Department      = var.dept
    Project         = var.project
    Workflow-stage  = var.workflow_stage
    Terraform-Scope = var.scope_name
  }

  versioning {
      enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
      }
    }
  }

}
EOF

cp ${TMP_DIR}/terraform-s3-state.tf.template ${ROLE_DESTINATION_DIR}/files_to_generate_backend/terraform-s3-state.tf

}


##   versions.tf
versions_tf_create () {
     echo "INFO: Copying versions.tf file to ${ROLE_DESTINATION_DIR} subdirs files_to_generate_backend and backend_files_for_new_terraform_code "

     cat << EOF > ${TMP_DIR}/versions.tf.template

terraform {
  required_version = ">= 0.12"
}
EOF

cp ${TMP_DIR}/versions.tf.template ${ROLE_DESTINATION_DIR}/files_to_generate_backend/versions.tf
cp ${TMP_DIR}/versions.tf.template ${ROLE_DESTINATION_DIR}/backend_files_for_new_terraform_code/versions.tf

}

##   gen-vars.tf
gen_vars_tf_create () {
     echo "INFO: Copying vars.tf file to ${ROLE_DESTINATION_DIR} subdirs backend_files_for_new_terraform_code/ and files_to_generate_backend/"
     cat << EOF > ${TMP_DIR}/gen-vars.tf.template

// define  scope name for terraform code to be run
variable "scope_name" {
  type = string
  default = "SCOPE_NAME"
}

// define org: company or organization identifier
variable "org" {
  type = string
  default = "ORG"
}

// define dept: department or division of dompany or org
variable "dept" {
  type = string
  default = "DEPT"
}

// define project: project identifier: eg: billing, provisioning, reporting, crm
variable "project" {
  type = string
  default = "PROJ"
}

// define workflow_stage: mgmt poc sandbox dev qa staging production
variable "workflow_stage" {
  type = string
  default = "WORKFLOW_STAGE"
}


// define unique bucket name for S3 name space
variable "state_s3_bucket" {
  type = string
  default = "STATE_S3_BUCKET"
}

// define state s3 bucket region
variable "state_s3_region" {
  type = string
  default = "STATE_S3_REGION"
}

// define general region
variable "region" {
  type = string
  default = "GEN_REGION"
}

// define dynamodb lock table
variable "dynamodb_lock_table" {
  type = string
  default = "DYNAMODB_LOCK_TABLE"
}
EOF

cat  ${TMP_DIR}/gen-vars.tf.template | sed -e "s/ORG/${ORG}/g" |  sed -e "s/DEPT/${DEPT}/g" | sed -e "s/PROJ/${PROJ}/g" | sed -e "s/WORKFLOW_STAGE/${WORKFLOW_STAGE}/g"  | sed -e "s/STATE_S3_REGION/${STATE_S3_REGION}/g" | sed -e "s/STATE_S3_BUCKET/${STATE_S3_BUCKET}/g" | sed -e "s/DYNAMODB_LOCK_TABLE/${DYNAMODB_LOCK_TABLE}/g" | sed -e "s/SCOPE_NAME/${SCOPE_NAME}/g" | sed -e "s/GEN_REGION/${REGION}/g"  >  ${TMP_DIR}/gen-vars.tf

cp ${TMP_DIR}/gen-vars.tf ${ROLE_DESTINATION_DIR}/files_to_generate_backend/vars.tf
cp ${TMP_DIR}/gen-vars.tf ${ROLE_DESTINATION_DIR}/backend_files_for_new_terraform_code/vars.tf

}


##   backends.tf
backends_tf_create () {
     echo "INFO: Copying backends.tf file to ${ROLE_DESTINATION_DIR}/backend_files_for_new_terraform_code/"
     cat << EOF > ${TMP_DIR}/backends.tf.template
terraform {
  backend "s3" {
    bucket = "STATE_S3_BUCKET"
    key    = "S3_TF_STATE_KEY_PATH"
    region = "STATE_S3_REGION"
    dynamodb_table = "DYNAMODB_LOCK_TABLE"
  }
}
EOF
cat ${TMP_DIR}/backends.tf.template | sed -e "s/STATE_S3_BUCKET/${STATE_S3_BUCKET}/g" | sed -e "s+S3_TF_STATE_KEY_PATH+${S3_TF_STATE_KEY_PATH}+g" | sed -e "s/STATE_S3_REGION/${STATE_S3_REGION}/g" | sed -e "s/DYNAMODB_LOCK_TABLE/${DYNAMODB_LOCK_TABLE}/g" > ${ROLE_DESTINATION_DIR}/backend_files_for_new_terraform_code/backends.tf


}


remove_tmp_dirs () {
     echo "INFO: Removing TMP dirs"
     for DIR_TO_REMOVE in `ls -d /tmp/* | grep gen-backend-tempfiles`
       do
           rm -rf ${DIR_TO_REMOVE}
       done
}




#######   MAIN PROGRAM  ###################

## create_backend_generation_directories
create_dirs

##   provider.tf
provider_tf_create
   
##   terraform-dynamodb.tf
terraform_dynamodb_tf_create

##   terraform-s3-state.tf
terraform_s3_state_tf_create

##   versions.tf
versions_tf_create

##   gen-vars.tf
gen_vars_tf_create

##   backends.tf
backends_tf_create

## remove backend_tmpdirs
#remove_tmp_dirs

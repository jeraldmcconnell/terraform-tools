# README #

This repository contains helper tools for using Terraform

This repo contains:

* role-specfic-tf-s3-dyndb-backend-generator/ 

   Bash script that generates a role specific S3 key and DynamoDB table for 
   locking and storing remote Terraform state files.  This is part of a strategy
   using tiered heirchial uniqueness of Terraform states to limit the risk of
   Terraform states being corrupted or lost at scale, as well as isolating the
   impacts of terraform destroy operations.

   This is to be used in conjunction with a heirchial or layered approach to implementing
   Terraform.

